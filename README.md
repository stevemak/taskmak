# taskmak

I have implemented all the functionality as per the specification. However I did make small changes

1. Added security to the API calls. Every API call will be supplied with authentication details as below:
    Username: test
    Password: test
2. Separated controller classes into two which changed the base path for task APIs
3. Added a few parameters to the API calls e.g When creating a task the following object will be send:

    {
        "description" : "Creating REST endpoints",
        "sprintId" : "1",
        "statusId" : "PEN",
        "labelId" : "Dev",
        "userId" : 1
    }
    
4. Implemented a Response object which wraps the response data with status codes eg.
    
    {
        "code": -1,
        "name": "SUCCESS",
        "description": "Success",
        "data": [
            [
                {
                    "userId": 1,
                    "username": "test",
                    "lastname": "test",
                    "firstname": "test",
                    "password": "password1",
                    "dateCreated": "2018-12-18T21:40:43.000+0000",
                    "dateUpdated": "2018-12-18T21:43:08.000+0000"
                }
            ]
        ]   
     }
     
      or when there is an error
     
     {
         "code": 2002,
         "name": "ERROR",
         "description": "Record not found"
     }
    
5. Added a few tables with seeded data so as to add few concepts to the task.
6. For delete API calls had to change the database record status to DELETED than delete the actual record.


Wishlist

1. Swagger documentation
2. Password encryption

