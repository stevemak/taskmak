package com.stevekmak.controller;

import com.stevekmak.dao.UserRepository;
import com.stevekmak.model.User;
import com.stevekmak.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by steven k makunzva.
 * Class that exposes REST APIs for managing Users
 */
@RestController
@RequestMapping("api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @GetMapping
    public Response getAllUsers() {
        return userRepository.getAllUsers();
    }

    @PostMapping
    public Response createUser(@RequestBody User  user) {
        return userRepository.createUser(user);
    }

    @PostMapping("/{id}")
    public Response updateUser(@RequestBody User  user, @PathVariable("id") Integer userId) {
        return userRepository.updateUser(user, userId);
    }

    @GetMapping("/{id}")
    public Response getUser(@PathVariable("id") Integer userId) {
        return userRepository.getUser(userId);
    }
}
