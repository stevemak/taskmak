package com.stevekmak.controller;

import com.stevekmak.dao.TaskRepository;
import com.stevekmak.model.Task;
import com.stevekmak.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by steven k makunzva.
 * Class that exposes REST APIs for managing tasks
 */
@RestController
@RequestMapping("api/task")
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @PostMapping
    public Response createTask(@RequestBody Task task) {
        return taskRepository.createTask(task);
    }

    @PostMapping("/{task_id}/user/{user_id}")
    public Response updateTask(@PathVariable("task_id") Integer taskId, @PathVariable("user_id") Integer userId, @RequestBody Task task) {
        return taskRepository.updateTask(task, taskId, userId);
    }

    @DeleteMapping("/{task_id}/user/{user_id}")
    public Response deleteTask(@PathVariable("user_id") Integer userId, @PathVariable("task_id") Integer taskId) {
        return taskRepository.deleteUserTask(taskId, userId);
    }

    @GetMapping("/{task_id}/user/{user_id}")
    public Response getUserTask(@PathVariable("user_id") Integer userId, @PathVariable("task_id") Integer taskId) {
        return taskRepository.getUserTask(taskId, userId);
    }

    @GetMapping("/user/{user_id}")
    public Response getAllUserTasks(@PathVariable("user_id") Integer userId) {
        return taskRepository.getUserTasks(userId);
    }
}
