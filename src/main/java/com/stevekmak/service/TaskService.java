package com.stevekmak.service;

import com.stevekmak.model.Task;
import org.springframework.data.repository.Repository;
import java.util.List;

/**
 * Created by steven k makunzva.
 */
public interface TaskService extends Repository<Task, Integer> {

    List<Task> findByUserId(Integer userId);
    Task save(Task entity);
    Task findById(Integer taskId);
    Task findByTaskIdAndUserId(Integer taskId, Integer userId);
    List<Task> findByStatusId(String statusId);
}
