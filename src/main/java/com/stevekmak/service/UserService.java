package com.stevekmak.service;

import com.stevekmak.model.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by steven k makunzva.
 */
public interface UserService extends CrudRepository<User, Integer> {
}
