package com.stevekmak.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by steven k makunzva.
 * Class that specifies the Task model. It references two tables 1. task : being the primary table 2. user_task: being secondary table
 */
@Entity
@Table(name = "task")
@SecondaryTable(name = "user_task")
public class Task {

    @Id
    @Column(name = "task_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer taskId;

    private String description;

    @Column(name = "sprint_id")
    private Integer sprintId;

    @Column(name = "status_id")
    private String statusId;

    @Column(name = "date_updated")
    private Date dateUpdated;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "label_id")
    private String labelId;

    @Column(name = "user_id", table = "user_task")
    private Integer userId;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public String getLabelId() {
        return labelId;
    }

    public void setLabelId(String labelId) {
        this.labelId = labelId;
    }

    public Integer getSprintId() {
        return sprintId;
    }

    public void setSprintId(Integer sprintId) {
        this.sprintId = sprintId;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
