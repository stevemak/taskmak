package com.stevekmak.response;

/**
 * Created by steven k makunzva.
 */
public enum EStatus {

    DONE("DN"), DELETED("DEL"), PENDING("PEN");

    private final String name;

    EStatus(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
