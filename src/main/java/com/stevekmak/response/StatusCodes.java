package com.stevekmak.response;

/**
 * Created by steven k makunzva.
 */
public enum StatusCodes {

    SUCCESS(-1, "SUCCESS", "Success"),
    ERROR_DELETE_RECORD(2000, "ERROR", "Failed to delete record"),
    ERROR_UPDATE_RECORD(2001, "ERROR", "Failed to update record"),
    RECORD_NOT_FOUND(2002, "ERROR", "Record not found"),
    RECORD_NOT_CREATED(2003, "ERROR", "Record not created"),
    RECORDS_NOT_FOUND(2002, "ERROR", "No records found");

    private final int code;
    private final String status;
    private final String description;

    StatusCodes(int code, String status, String description) {
        this.code = code;
        this.status = status;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
