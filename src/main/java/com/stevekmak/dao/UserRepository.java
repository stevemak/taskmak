package com.stevekmak.dao;

import com.stevekmak.model.User;
import com.stevekmak.response.Response;
import com.stevekmak.response.StatusCodes;
import com.stevekmak.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by steven k makunzva.
 */
@Service
public class UserRepository {

    @Autowired
    UserService userService;
    Response response;
    List data;

    /**
     * Method that return a list of all the users
     * @return Response Object which contains the status code and data
     */
    public Response getAllUsers() {
        response = new Response();
        List users = (List<User>) userService.findAll();
        data = new ArrayList();
        data.add(users);

        if(users.size() > 0){
            response.setCode(StatusCodes.SUCCESS.getCode());
            response.setName(StatusCodes.SUCCESS.getStatus());
            response.setDescription(StatusCodes.SUCCESS.getDescription());
            response.setData(data);
        } else {
            response.setCode(StatusCodes.RECORDS_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORDS_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORDS_NOT_FOUND.getDescription());
        }
        return response;
    }

    /**
     * Method that creates a new user
     * @param user User object to be saved in the database
     * @return Response Object which contains the status code
     */
    public Response createUser(User user) {
        user.setDateCreated(new Date());
        response = new Response();

        try {
            if(userService.save(user).getUserId() != 0) {
                response.setCode(StatusCodes.SUCCESS.getCode());
                response.setName(StatusCodes.SUCCESS.getStatus());
                response.setDescription(StatusCodes.SUCCESS.getDescription());
            } else {
                response.setCode(StatusCodes.RECORD_NOT_CREATED.getCode());
                response.setName(StatusCodes.RECORD_NOT_CREATED.getStatus());
                response.setDescription(StatusCodes.RECORD_NOT_CREATED.getDescription());
            }

        } catch (Exception e) {
            response.setCode(StatusCodes.RECORD_NOT_CREATED.getCode());
            response.setName(StatusCodes.RECORD_NOT_CREATED.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_CREATED.getDescription());
            e.printStackTrace();
        }

        return response;
    }

    /**
     * Method for updating an existing user
     * @param user User object to be updated
     * @param userId Id of the user to be updated
     * @return Response Object which contains the status code
     */
    public Response updateUser(User user, Integer userId) {
        response = new Response();
        try {
            User updateUser = userService.findById(userId).get();

            if(updateUser != null) {
                if(user.getFirstname() != null) {
                    updateUser.setFirstname(user.getFirstname());
                }

                if(user.getLastname() != null) {
                    updateUser.setLastname(user.getLastname());
                }

                if(user.getUsername() != null) {
                    updateUser.setUsername(user.getUsername());
                }

                if(user.getPassword() != null) {
                    updateUser.setPassword(user.getPassword());
                }

                updateUser.setDateUpdated(new Date());

                try {
                    if(userService.save(updateUser).getUserId() != 0) {
                        response.setCode(StatusCodes.SUCCESS.getCode());
                        response.setName(StatusCodes.SUCCESS.getStatus());
                        response.setDescription(StatusCodes.SUCCESS.getDescription());
                    } else {
                        response.setCode(StatusCodes.ERROR_UPDATE_RECORD.getCode());
                        response.setName(StatusCodes.ERROR_UPDATE_RECORD.getStatus());
                        response.setDescription(StatusCodes.ERROR_UPDATE_RECORD.getDescription());
                    }
                } catch (Exception e) {
                    response.setCode(StatusCodes.ERROR_UPDATE_RECORD.getCode());
                    response.setName(StatusCodes.ERROR_UPDATE_RECORD.getStatus());
                    response.setDescription(StatusCodes.ERROR_UPDATE_RECORD.getDescription());
                    e.printStackTrace();
                }
            } else {
                response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
                response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
                response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
            }

        } catch (Exception e) {
            response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
            e.printStackTrace();
        }

        return response;
    }

    /**
     * Method that returns a specific user
     * @param userId Id of the user
     * @return Response Object which contains the status code and data
     */
    public Response getUser(Integer userId) {
        response = new Response();
        data = new ArrayList();

        try {
            User user = userService.findById(userId).get();

            if(user != null) {
                response.setCode(StatusCodes.SUCCESS.getCode());
                response.setName(StatusCodes.SUCCESS.getStatus());
                response.setDescription(StatusCodes.SUCCESS.getDescription());
                data.add(user);
                response.setData(data);
            } else {
                response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
                response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
                response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
            }
        } catch(Exception e) {
            response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
            e.printStackTrace();
        }

        return response;
    }
}
