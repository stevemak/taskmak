package com.stevekmak.dao;

import com.stevekmak.response.EStatus;
import com.stevekmak.model.Task;
import com.stevekmak.response.Response;
import com.stevekmak.response.StatusCodes;
import com.stevekmak.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by steven k makunzva.
 */
@Service
public class TaskRepository {

    @Autowired
    TaskService taskService;
    Response response;
    List data;

    /**
     * Method for creating a task
     * @param task Task object to be created
     * @return Response Object which contains the status code
     */
    public Response createTask(Task task) {
        response = new Response();

        try{
            task.setDateCreated(new Date());
            if(taskService.save(task).getTaskId() != 0) {
                response.setCode(StatusCodes.SUCCESS.getCode());
                response.setName(StatusCodes.SUCCESS.getStatus());
                response.setDescription(StatusCodes.SUCCESS.getDescription());
            } else {
                response.setCode(StatusCodes.RECORD_NOT_CREATED.getCode());
                response.setName(StatusCodes.RECORD_NOT_CREATED.getStatus());
                response.setDescription(StatusCodes.RECORD_NOT_CREATED.getDescription());
            }
        } catch (Exception e) {
            response.setCode(StatusCodes.RECORD_NOT_CREATED.getCode());
            response.setName(StatusCodes.RECORD_NOT_CREATED.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_CREATED.getDescription());
            e.printStackTrace();
        }

        return response;
    }

    /**
     * Method for updating a specific task associated to a particular user
     * @param task Task object to be updated
     * @param taskId Id of the task that should be updated
     * @param userId Id of the user that is associated to the task
     * @return Response Object which contains the status code
     */
    public Response updateTask(Task task, Integer taskId, Integer userId) {
        response = new Response();
        Task findTask = taskService.findByTaskIdAndUserId(taskId, userId);

        if(findTask != null) {

            if(task.getLabelId() != null) {
                findTask.setLabelId(task.getLabelId());
            }

            if(task.getSprintId() != null) {
                findTask.setSprintId(task.getSprintId());
            }

            if(task.getStatusId() != null) {
                findTask.setStatusId(task.getStatusId());
            }

            if(task.getUserId() != null) {
                findTask.setUserId(task.getUserId());
            }

            if(task.getDescription() != null) {
                findTask.setDescription(task.getDescription());
            }

            findTask.setDateUpdated(new Date());

            try {

                if(taskService.save(findTask).getTaskId() != 0) {
                    response.setCode(StatusCodes.SUCCESS.getCode());
                    response.setName(StatusCodes.SUCCESS.getStatus());
                    response.setDescription(StatusCodes.SUCCESS.getDescription());
                } else {
                    response.setCode(StatusCodes.ERROR_UPDATE_RECORD.getCode());
                    response.setName(StatusCodes.ERROR_UPDATE_RECORD.getStatus());
                    response.setDescription(StatusCodes.ERROR_UPDATE_RECORD.getDescription());
                }
            } catch(Exception e) {
                response.setCode(StatusCodes.ERROR_UPDATE_RECORD.getCode());
                response.setName(StatusCodes.ERROR_UPDATE_RECORD.getStatus());
                response.setDescription(StatusCodes.ERROR_UPDATE_RECORD.getDescription());
                e.printStackTrace();
            }

        } else {
            response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
        }
        return response;
    }

    /**
     * Get all the tasks associated with a particular user
     * @param userId Id of the user
     * @return Response Object which contains the status code and data
     */
    public Response getUserTasks(Integer userId) {
        response = new Response();
        List<Task> tasks = taskService.findByUserId(userId);

        if(tasks.size() > 0) {
            response.setCode(StatusCodes.SUCCESS.getCode());
            response.setName(StatusCodes.SUCCESS.getStatus());
            response.setDescription(StatusCodes.SUCCESS.getDescription());
            response.setData(tasks);
        } else {
            response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
        }
        return response;
    }

    /**
     * Method for retrieving a specific task for a particular user
     * @param taskId Id of the specific task
     * @param userId Id of the user
     * @return Response Object which contains the status code and data
     */
    public Response getUserTask(Integer taskId, Integer userId) {
        response = new Response();
        Task task = taskService.findByTaskIdAndUserId(taskId, userId);

        if(task != null) {
            response.setCode(StatusCodes.SUCCESS.getCode());
            response.setName(StatusCodes.SUCCESS.getStatus());
            response.setDescription(StatusCodes.SUCCESS.getDescription());
            data = new ArrayList();
            data.add(task);
            response.setData(data);
        } else {
            response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
        }
        return response;
    }

    /**
     * Method for deleting a task for a specific user. NB: This is achieved by setting the task status to deleted
     * @param taskId Id of the task to be deleted
     * @param userId Id of the user
     * @return Response Object which contains the status code and data
     */
    public Response deleteUserTask(Integer taskId, Integer userId) {
        response = new Response();
        Task findTask = taskService.findByTaskIdAndUserId(taskId, userId);

        if(findTask != null) {
            findTask.setStatusId(EStatus.DELETED.getName());
            findTask.setDateUpdated(new Date());

            try {

                if(taskService.save(findTask).getTaskId() != 0) {
                    response.setCode(StatusCodes.SUCCESS.getCode());
                    response.setName(StatusCodes.SUCCESS.getStatus());
                    response.setDescription(StatusCodes.SUCCESS.getDescription());
                } else {
                    response.setCode(StatusCodes.ERROR_DELETE_RECORD.getCode());
                    response.setName(StatusCodes.ERROR_DELETE_RECORD.getStatus());
                    response.setDescription(StatusCodes.ERROR_DELETE_RECORD.getDescription());
                }
            } catch (Exception e) {
                response.setCode(StatusCodes.ERROR_DELETE_RECORD.getCode());
                response.setName(StatusCodes.ERROR_DELETE_RECORD.getStatus());
                response.setDescription(StatusCodes.ERROR_DELETE_RECORD.getDescription());
                e.printStackTrace();
            }

        } else {
            response.setCode(StatusCodes.RECORD_NOT_FOUND.getCode());
            response.setName(StatusCodes.RECORD_NOT_FOUND.getStatus());
            response.setDescription(StatusCodes.RECORD_NOT_FOUND.getDescription());
        }

        return response;
    }

    /**
     * Scheduled task which checks whether there are tasks in pending state and past current date.
     * The tasks are then updated to done.
     */
    @Scheduled(fixedRate = 5000)  //5 seconds is just used for testing purposes 24hrs or more is the realistic value
    @Transactional
    public void getPendingTasks() {
        List<Task> byUserId = taskService.findByStatusId(EStatus.PENDING.getName());

        for(Task task : byUserId) {

            if(task.getDateCreated().before(new Date())) {
                System.out.println("\n taskId : " + task.getTaskId() + "\n description : " + task.getDescription() + " \n labelId: " + task.getLabelId()
                        + " \nstatusId: " + task.getStatusId() + " \n sprintId : " + task.getSprintId() + " \n dateCreated: " + task.getDateCreated());

                task.setStatusId(EStatus.DONE.getName());
                taskService.save(task);
            }
        }
    }
}
