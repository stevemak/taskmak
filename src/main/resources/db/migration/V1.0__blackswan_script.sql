CREATE TABLE `blackswan`.`user` (
  `user_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `user_name` VARCHAR(100) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` DATETIME NULL,
  PRIMARY KEY (`user_id`, `user_name`),
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC) VISIBLE);

CREATE TABLE `blackswan`.`status` (
  `id` VARCHAR(10) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `blackswan`.`label` (
  `id` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `blackswan`.`sprint` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  `duration` INT NOT NULL COMMENT 'Sprint duration in wks',
  `status_id` VARCHAR(45) NOT NULL,
  `date_started` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_statusId_idx` (`status_id` ASC) VISIBLE,
  CONSTRAINT `FK_statusId`
  FOREIGN KEY (`status_id`)
  REFERENCES `blackswan`.`status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);



CREATE TABLE `blackswan`.`task` (
  `task_id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(100) NOT NULL,
  `label_id` VARCHAR(45) NOT NULL,
  `sprint_id` INT NOT NULL,
  `status_id` VARCHAR(45) NOT NULL ,
  `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_updated` DATETIME NULL,
  PRIMARY KEY (`task_id`),
  INDEX `FK_task_status_id_idx` (`status_id` ASC) VISIBLE,
  INDEX `FK_task_labelId_idx` (`label_id` ASC) VISIBLE,
  INDEX `FK_task_sprint_id_idx` (`sprint_id` ASC) VISIBLE,
  CONSTRAINT `FK_task_label_id`
    FOREIGN KEY (`label_id`)
    REFERENCES `blackswan`.`label` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_task_sprint_id`
    FOREIGN KEY (`sprint_id`)
    REFERENCES `blackswan`.`sprint` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_task_status_id`
  FOREIGN KEY (`status_id`)
  REFERENCES `blackswan`.`status` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION);


CREATE TABLE `blackswan`.`user_task` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `task_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `FK_userId_idx` (`user_id` ASC) VISIBLE,
  INDEX `FK_user_task_task_id_idx` (`task_id` ASC) VISIBLE,
  CONSTRAINT `FK_user_id`
  FOREIGN KEY (`user_id`)
  REFERENCES `blackswan`.`user` (`user_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_user_task_task_id`
  FOREIGN KEY (`task_id`)
  REFERENCES `blackswan`.`task` (`task_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `blackswan`.`label`
(`id`,`description`)
VALUES
('Dev','Developer'),
('BA', 'Business Analyst'),
('QA', 'Quality Analyst');

INSERT INTO `blackswan`.`status`
(`id`,`description`)
VALUES
('DN','Done'),
('PEN', 'Pending'),
('DEL', 'Deleted'),
('PR', 'Progress'),
('OP', 'Open'),
('CL', 'Closed');

INSERT INTO `blackswan`.`sprint`
(`description`,`duration`,`status_id`,`date_started`,`date_end`)
VALUES
('Sprint 1',2,'OP',NOW(),DATE_ADD(now(),INTERVAL 2 WEEK));